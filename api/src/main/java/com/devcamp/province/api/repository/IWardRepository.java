package com.devcamp.province.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.province.api.model.Ward;

public interface IWardRepository extends JpaRepository<Ward,Long>{
    
}
