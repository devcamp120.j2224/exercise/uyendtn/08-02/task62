package com.devcamp.province.api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.province.api.model.District;
import com.devcamp.province.api.model.Province;
import com.devcamp.province.api.model.Ward;
import com.devcamp.province.api.repository.IDistrictRepository;
import com.devcamp.province.api.repository.IProvinceRepository;
import com.devcamp.province.api.repository.IWardRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class Controller {
    @Autowired
    IWardRepository iWardRepository;

    @Autowired
    IDistrictRepository iDistrictRepository;

    @Autowired
    IProvinceRepository iProvinceRepository;

    @GetMapping("/provinces")
    public ResponseEntity<List<Province>> getProvinceList() {
        try {
            List<Province> provinces = new ArrayList<Province>();
            iProvinceRepository.findAll().forEach(provinces::add);

            return new ResponseEntity<>(provinces, HttpStatus.OK);
            
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts")
    public ResponseEntity<Set<District>> getDistrictsList(@RequestParam(name = "provinceId", required = true) int provinceId) {
        try {
            Province provinceCheck = iProvinceRepository.findAllById(provinceId);
            if (provinceCheck!=null){
               return new ResponseEntity<>(provinceCheck.getDistricts(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/wards")
    public ResponseEntity<Set<Ward>> getWardList(@RequestParam(name = "districtId", required = true) int districtId) {
        try {
            District districtCheck = iDistrictRepository.findAllById(districtId);
            if (districtCheck!=null){
               return new ResponseEntity<>(districtCheck.getWards(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
