package com.devcamp.province.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.province.api.model.Province;

public interface IProvinceRepository extends JpaRepository<Province,Long> {

    Province findAllById(int provinceId);
    
}
