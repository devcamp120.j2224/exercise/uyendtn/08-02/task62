package com.devcamp.province.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.province.api.model.District;

public interface IDistrictRepository extends JpaRepository<District,Long> {

    District findAllById(int districtId);
    
}
